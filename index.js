const present = require('present');
const start = present();
const async = require('async');
const config = require('./config');
const fs = require('fs');
const es = require('elasticsearch');
const mapping = require("./mapping").structure;
const settings = require("./settings").structure;
const csv = require("fast-csv");
const csvFile   = "./data/data.csv";
const bulkLimit = config.bullkSettings.limit;

const elasticInstance = new es.Client({
    host: config.elasticSearch.host,
    indice: config.elasticSearch.index,
    type: config.elasticSearch.type,
    settings: settings,
    mapping: mapping,
    debug: config.global.debug
});

async.waterfall(
    [
        deleteIndex,
        readCSV,
        prepareData,
        bulkES
    ],
    function (err) {
        if (err) throw new Error(err);
        else console.log("Fin de la carga", "\t\t\t", new Date());
      
        var end = present();
        var minutes = Math.floor((end - start) / 60000);
        var seconds = (((end - start) % 60000) / 1000).toFixed(0);
      
        console.log('Tiempo total: ' + minutes + ":" + (seconds < 10 ? '0' : '') + seconds);
        
        process.exit();
    }
);

function deleteIndex (callback) {
    var elasticInstance = new es.Client({
        host: config.elasticSearch.host,
        indice: config.elasticSearch.index,
        type: '?',
        settings: settings,
        mapping: mapping,
        debug: config.global.debug
    });

    console.log('Creando indice', elasticInstance.indice);

    elasticInstance.createIndice(function (err) {
        if (err) callback(err);
        else callback(null);
    });
}

function readCSV (callback) {
    console.log('Leyendo CSV ', "\t\t\t", new Date());

    var readStream = fs.createReadStream(csvFile, { encoding: "binary" });
    var result = [];

    var parser = csv.fromStream(readStream, { headers: false, delimiter: ',' }).on("data", function (item) {
        parser.pause();

        result.push({
            pais: item[0],
            institucion: item[1],
            ranking: item[2],
            puntaje: item[3]
        });

        parser.resume();
    }).on("end", function () {
        callback(null, result);
    }).on("error", function (err) {
        if (err) callback(err);
    });
}

function prepareData (result, callback) {
    console.log('Hay', result.length, 'registros');

    callback(null, 0, bulkLimit - 1, result);
}

function bulkES (inicio, fin, items, callback) {
    var dataBulk = [];
    var out = false;

    for (var i = inicio; i <= fin; i++) {
        if (items[i] === undefined) {
            out = true;
            break;
        } else {
            dataBulk.push(items[i]);
        }
    }

    if (dataBulk.length > 0 && !out) {
        console.log('Insertando', dataBulk.length);

        elasticInstance.load(dataBulk, function (err) {
            if (err) callback(err);
            else {
                inicio = fin + 1;
                fin += bulkLimit;

                bulkES(inicio, fin, items, callback);
            }

        });

    } else if (out) {
        console.log('Insertando últimos ', dataBulk.length);

        if (dataBulk.length > 0) {
            elasticInstance.load(dataBulk, function (err) {
                if(err) callback(err);
                else callback(null);
            });

        } else {
            callback(null);
        }
    }
}
