exports.structure = {
  instituciones:{
    _all: {
      analyzer: 'nGram_analyzer',
      search_analyzer: 'whitespace_analyzer'
    },
    properties: {
      institucion: {
        type: 'text',
        index: 'not_analyzed'
      },
      pais: {
        type: 'text',
        index: 'no',
        include_in_all: false
      },
      puntaje: {
        type: 'text',
        index: 'no',
        include_in_all: false
      },
      ranking: {
        type: 'long',
        index: 'no',
        include_in_all: false
      }
    }
  }
};
