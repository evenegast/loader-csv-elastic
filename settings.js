
exports.structure = {
  "analysis": {
      "filter": {
          "nGram_filter": {
              "type": "nGram",
              "min_gram": 1,
              "max_gram": 15,
              "token_chars": [
                  "letter",
                  "digit",
                  "punctuation",
                  "symbol"
              ]
          },
          "shingle": {
              "type": "shingle",
              "min_shingle_size": 2,
              "max_shingle_size": 3
          }
      },
      "analyzer": {
          "trigram": {
              "type": "custom",
              "tokenizer": "standard",
              "filter": ["standard", "shingle"]
          },
          "nGram_analyzer": {
              "type": "custom",
              "tokenizer": "whitespace",
              "filter": [
                  "lowercase",
                  "asciifolding",
                  "nGram_filter"
              ]
          },
          "whitespace_analyzer": {
              "type": "custom",
              "tokenizer": "whitespace",
              "filter": [
                  "lowercase",
                  "asciifolding"
              ]
          }
      }
  }
};
