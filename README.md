# README 🚀 #

Esta es una prueba de una carga masiva desarrollada en **NodeJS**. Carga data desde un CSV a Elasticsearch.

Versión 1.0 Erick Venegas Toledo **erick.venegas.toledo@gmail.com**

### NodeJS 🛠️ ###

* Se utiliza la versión 12.16.1 de NodeJS.

### Elasticsearch 🛠️ ###

* **Elasticsearch** es un motor de analitica o análisis distribuido, este permite almacenar datos para realiza una búsqueda inteligente, bajo los parámetros dado en sus archivos de configuración.

**Importante** Elasticsearch no es una base de datos, solo son índices de búsqueda de datos.

### Consideraciones 📋 ###

* Se debe crear un archivo de configuración **config.js**, tal como es el **config.js.sample**.
* Se debe crear un enlace con elasticsearch para utilizar su ruta.
* Se debe ingresar el index, que es el índice donde serán alojados los datos.
* Se debe ingresar el tipo, nombre cualquiera, que haga referencia a los que está cargando.

### Inicializar Backend 🔧 ###

* Luego de descargar del repositorio el proyecto se debe inicializar.
* npm install
* node index.js
